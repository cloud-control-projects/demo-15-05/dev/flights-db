variable "instance_cores" {
  type    = number
}

variable "instance_memory" {
  type    = number
}

variable "platform" {
  type = string
}

variable "port" {
  type    = number
}

variable "vpc_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "name" {
  type = string
}

variable "module_path" {
  type = string
}

variable "image" {
  type = string
}

variable "env" {
  type = list(object({
    name : string
    value : any
  }))
}

variable "folder_id" {
  type = string
}

variable "zone" {
  type = string
}

variable "docker_logs" {
  type = bool
}

variable "extended_metrics" {
  type = bool
}

variable "log_group_id" {
  type = string
  default = ""
}
