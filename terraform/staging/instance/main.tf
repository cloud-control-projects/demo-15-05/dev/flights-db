terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.91.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "3.4.0"
    }
  }
  required_version = ">= 0.13"
}

locals {
  extended_metrics_cmd = var.extended_metrics ? "runcmd:\n  - wget -O - https://monitoring.api.cloud.yandex.net/monitoring/v2/unifiedAgent/config/install.sh | bash" : ""
}

data "yandex_compute_image" "container-optimized-image" {
  family = "container-optimized-image"
}

resource "tls_private_key" "yandex-keys" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "local_file" "cloud_config_file" {
  count    = var.docker_logs ? 0 : 1
  filename = "${var.module_path}/files/cloud_config.yaml"
  content  = <<-EOF
      #cloud-config
      ${local.extended_metrics_cmd}
      ssh_pwauth: no
      users:
        - name: yc-user
          sudo: ALL=(ALL) NOPASSWD:ALL
          shell: /bin/bash
          ssh-authorized-keys:
            - "${tls_private_key.yandex-keys.public_key_openssh}"
    EOF
}

resource "local_file" "declaration_file" {
  count    = var.docker_logs ? 0 : 1
  filename = "${var.module_path}/files/declaration.yaml"
  content  = <<-EOF
      spec:
        containers:
        - image: ${var.image}
          securityContext:
            privileged: false
          stdin: false
          tty: false
          env:
${join("\n", [
  for env_var in var.env :
    "          - name: ${env_var.name}\n            value: ${env_var.value}"
])}
    EOF
}

resource "local_file" "spec_file" {
  count    = var.docker_logs ? 1 : 0
  filename = "${var.module_path}/files/spec.yaml"
  content  = <<-EOF
      version: '3.7'
      services:
        app:
          container_name: app
          image: ${var.image}
          restart: always
          ports:
            - "${var.port}:${var.port}"
          environment:
${join("\n", [
  for env_var in var.env :
    "            ${env_var.name}: ${env_var.value}"
])}
          depends_on:
            - fluentbit
          logging:
            driver: fluentd
            options:
              fluentd-address: localhost:24224
              tag: app.logs

        fluentbit:
          container_name: fluentbit
          image: cr.yandex/yc/fluent-bit-plugin-yandex:v1.0.3-fluent-bit-1.8.6
          ports:
            - 24224:24224
            - 24224:24224/udp
          restart: always
          environment:
            YC_GROUP_ID: ${var.log_group_id}
          volumes:
            - /etc/fluentbit/fluentbit.conf:/fluent-bit/etc/fluent-bit.conf
            - /etc/fluentbit/parsers.conf:/fluent-bit/etc/parsers.conf
    EOF
}

resource "local_file" "user_data_file" {
  filename = "${var.module_path}/files/user_data.yaml"
  content  = <<-EOF
      #cloud-config
      ${local.extended_metrics_cmd}
      write_files:
        - content: |
            [SERVICE]
                Flush         1
                Log_File      /var/log/fluentbit.log
                Log_Level     error
                Daemon        off
                Parsers_File  /fluent-bit/etc/parsers.conf

            [FILTER]
                Name parser
                Match app.logs
                Key_Name log
                Parser app_log_parser
                Reserve_Data On

            [INPUT]
                Name              forward
                Listen            0.0.0.0
                Port              24224
                Buffer_Chunk_Size 1M
                Buffer_Max_Size   6M

            [OUTPUT]
                Name            yc-logging
                Match           *
                resource_type   instance_ComputeInstance
                message_key     text
                group_id        ${var.log_group_id}
                level_key       severity
                default_level   WARN
                authorization   instance-service-account
          path: "/etc/fluentbit/fluentbit.conf"
        - content: |
            [PARSER]
                Name   app_log_parser
                Format regex
                Regex  ^\[req_id=(?<req_id>[0-9a-fA-F\-]+)\] \[(?<severity>.*)\] (?<code>\d+) (?<text>.*)$
                Types  code:integer
          path: "/etc/fluentbit/parsers.conf"

      ssh_pwauth: no
      users:
        - name: yc-user
          sudo: ALL=(ALL) NOPASSWD:ALL
          shell: /bin/bash
          ssh-authorized-keys:
            - "${tls_private_key.yandex-keys.public_key_openssh}"
    EOF
}

data "local_file" "cloud_config" {
  count      = var.docker_logs ? 0 : 1
  filename   = "${var.module_path}/files/cloud_config.yaml"
  depends_on = [local_file.cloud_config_file]
}

data "local_file" "declaration" {
  count      = var.docker_logs ? 0 : 1
  filename   = "${var.module_path}/files/declaration.yaml"
  depends_on = [local_file.declaration_file]
}

data "local_file" "spec" {
  count      = var.docker_logs ? 1 : 0
  filename   = "${var.module_path}/files/spec.yaml"
  depends_on = [local_file.spec_file]
}

data "local_file" "user_data" {
  count      = var.docker_logs ? 1 : 0
  filename   = "${var.module_path}/files/user_data.yaml"
  depends_on = [local_file.user_data_file]
}

resource "yandex_iam_service_account" "service_account" {
  name = "${var.name}-sa"
}

resource "yandex_resourcemanager_folder_iam_member" "admin" {
  folder_id = var.folder_id
  role      = "admin"
  member    = "serviceAccount:${yandex_iam_service_account.service_account.id}"

  depends_on = [yandex_iam_service_account.service_account]
}

resource "yandex_compute_instance_group" "instance_group" {
  name               = "${var.name}-instance-group"
  folder_id          = var.folder_id
  service_account_id = yandex_iam_service_account.service_account.id
  instance_template {
    service_account_id = yandex_iam_service_account.service_account.id

    platform_id = var.platform

    resources {
      cores  = var.instance_cores
      memory = var.instance_memory
    }

    boot_disk {
      initialize_params {
        image_id = data.yandex_compute_image.container-optimized-image.id
      }
    }

    network_interface {
      network_id         = var.vpc_id
      subnet_ids         = [var.subnet_id]
      nat                = true
      security_group_ids = [yandex_vpc_security_group.sg-instance.id]
    }

    metadata = {
      docker-compose               = var.docker_logs ? data.local_file.spec[0].content : null
      docker-container-declaration = var.docker_logs ? null : data.local_file.declaration[0].content
      user-data                    = var.docker_logs ? data.local_file.user_data[0].content : data.local_file.cloud_config[0].content
      ssh-keys                     = "yc-user:${tls_private_key.yandex-keys.public_key_openssh}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    zones = [var.zone]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  application_load_balancer {
    target_group_name = "target-group"
  }

  labels = {
    name       = "${var.name}-instance-group"
    managed-by = "terraform"
  }

  depends_on = [yandex_iam_service_account.service_account, yandex_resourcemanager_folder_iam_member.admin]
}

resource "yandex_vpc_security_group" "sg-instance" {
  name       = "${var.name}-instance-sg"
  network_id = var.vpc_id

  ingress {
    protocol       = "TCP"
    description    = "SSH"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 22
    to_port        = 22
  }

  ingress {
    protocol       = "TCP"
    description    = "HTTPS"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 443
    to_port        = 443
  }

  egress {
    protocol       = "ANY"
    description    = "ALL"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }

  labels = {
    name       = "${var.name}-instance-sg"
    managed-by = "terraform"
  }
}
