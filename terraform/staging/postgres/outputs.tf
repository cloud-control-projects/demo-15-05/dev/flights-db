output "host" {
  value = "c-${yandex_mdb_postgresql_cluster.cluster-db-postgresql-main.id}.rw.mdb.yandexcloud.net"
}

output "name" {
  value = yandex_mdb_postgresql_database.db-postgresql-main.name
}

output "port" {
  value = local.port
}

output "username" {
  value = yandex_mdb_postgresql_user.user-postgresql-main.name
}

output "password" {
  value = yandex_mdb_postgresql_user.user-postgresql-main.password
  sensitive = true
}
