terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.91.0"
    }
  }
  required_version = ">= 0.13"
}


resource "yandex_vpc_address" "static-address" {
  name = "${var.name}-static-address"

  external_ipv4_address {
    zone_id = var.zone
  }
}

resource "yandex_alb_backend_group" "backend-group" {
  name = "${var.name}-alb-backend-group"

  http_backend {
    name             = "${var.name}-alb-backend-group"
    port             = var.port
    target_group_ids = [var.target_group_id]
  }
}

resource "yandex_alb_http_router" "router" {
  name = "${var.name}-alb-router"
}

resource "yandex_alb_virtual_host" "virtual_host" {
  name           = "${var.name}-virtual-host"
  http_router_id = yandex_alb_http_router.router.id
  route {
    name = "${var.name}-alb-route"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.backend-group.id
      }
    }
  }
}

resource "yandex_alb_load_balancer" "alb" {
  name               = "${var.name}-alb"
  network_id         = var.vpc_id
  security_group_ids = [yandex_vpc_security_group.sg-alb.id]

  allocation_policy {
    location {
      zone_id   = var.zone
      subnet_id = var.subnet_id
    }
  }

  listener {
    name = "${var.name}-alb-listener"
    endpoint {
      address {
        external_ipv4_address {
          address = yandex_vpc_address.static-address.external_ipv4_address[0].address
        }
      }
      ports = [80]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.router.id
      }
    }
  }

  dynamic "log_options" {
    for_each = var.alb_logs ? [1] : []
    content {
      log_group_id = var.log_group_id
    }
  }

  labels = {
    name       = "${var.name}-alb"
    managed-by = "terraform"
  }
}

resource "yandex_vpc_security_group" "sg-alb" {
  name       = "${var.name}-alb-sg"
  network_id = var.vpc_id

  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol       = "TCP"
    description    = "ext-http"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 80
  }

  ingress {
    protocol       = "TCP"
    description    = "ext-https"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 443
  }

  ingress {
    protocol          = "TCP"
    description       = "healthchecks"
    predefined_target = "loadbalancer_healthchecks"
    port              = 30080
  }

  labels = {
    name       = "${var.name}-alb-sg"
    managed-by = "terraform"
  }
}

resource "yandex_vpc_security_group_rule" "rule80" {
  security_group_binding = var.instance_sg_id
  direction              = "ingress"
  from_port              = 80
  to_port                = 80
  protocol               = "TCP"
  description            = "HTTP"
  security_group_id      = yandex_vpc_security_group.sg-alb.id
}

resource "yandex_vpc_security_group_rule" "rule-app" {
  security_group_binding = var.instance_sg_id
  direction              = "ingress"
  from_port              = var.port
  to_port                = var.port
  protocol               = "TCP"
  description            = "HTTP"
  security_group_id      = yandex_vpc_security_group.sg-alb.id
}

