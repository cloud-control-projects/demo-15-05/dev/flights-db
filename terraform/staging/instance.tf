module "instance" {
    source = "./instance"

    image            = var.image
    module_path      = path.module
    name             = var.name
    subnet_id        = module.vpc.subnet_id
    vpc_id           = module.vpc.vpc_id
    env              = local.env
    folder_id        = var.folder_id
    zone             = var.zone
    docker_logs      = var.docker_logs
    extended_metrics = var.extended_metrics

    log_group_id     = var.docker_logs ? yandex_logging_group.log[0].id : null

    instance_cores   = var.instance_cores
    instance_memory  = var.instance_memory
    port             = var.port
    platform         = var.platform
}