package com.demo.flightsdb2.controller

import com.demo.flightsdb2.domain.Flight
import com.demo.flightsdb2.domain.FlightRepository
import com.demo.flightsdb2.dto.FlightDto
import com.demo.flightsdb2.dto.FlightMapper
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.OffsetTime
import java.time.ZoneOffset

@RestController
@RequestMapping("/api/flight")
class FlightController(private val flightRepository: FlightRepository, private val flightMapper: FlightMapper) {
    @GetMapping
    fun getList(@RequestParam from: Int, @RequestParam to: Int,
                @RequestParam dateMin: LocalDate, @RequestParam dateMax: LocalDate): List<FlightDto> {
        val midnight = OffsetTime.of(0, 0, 0, 0, ZoneOffset.UTC)
        val flights: List<Flight> = flightRepository.findByAirportsAndDates(from, to,
                dateMin.atTime(midnight),
                dateMax.atTime(midnight).plusDays(1)
        )
        val flightDtos: List<FlightDto> = flights.map(flightMapper::toDto)
        return flightDtos
    }
}

