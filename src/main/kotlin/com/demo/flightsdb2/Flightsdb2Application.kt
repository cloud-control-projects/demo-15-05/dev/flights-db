package com.demo.flightsdb2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class Flightsdb2Application

fun main(args: Array<String>) {
    runApplication<Flightsdb2Application>(*args)
}
