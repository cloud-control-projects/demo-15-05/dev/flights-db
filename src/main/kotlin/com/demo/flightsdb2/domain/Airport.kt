package com.demo.flightsdb2.domain

import jakarta.persistence.*
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size

@Entity
@Table(name = "airdatas_airport", indexes = [
    Index(name = "airdatas_airport_code_key", columnList = "code", unique = true)
])
open class Airport : NamedEntity() {

    @Size(max = 31)
    @NotNull
    @Column(name = "code", nullable = false, length = 31)
    open var code: String? = null

    @Column(name = "lat")
    open var lat: Double? = null

    @Column(name = "lon")
    open var lon: Double? = null
}