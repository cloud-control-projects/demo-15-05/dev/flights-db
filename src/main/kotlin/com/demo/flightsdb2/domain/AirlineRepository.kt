package com.demo.flightsdb2.domain;

import org.springframework.data.jpa.repository.JpaRepository

interface AirlineRepository : JpaRepository<Airline, Int> {
}