package com.demo.flightsdb2.dto

import com.demo.flightsdb2.domain.Airline
import org.mapstruct.*

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
abstract class AirlineMapper {

    abstract fun toEntity(airlineDto: AirlineDto): Airline

    abstract fun toDto(airline: Airline): AirlineDto

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    abstract fun partialUpdate(airlineDto: AirlineDto, @MappingTarget airline: Airline): Airline
}