create table airdatas_airport (
	id serial,
	name varchar(255) not null,
	code varchar(31) not null unique,
	lat double precision,
	lon double precision,
	primary key (id)
);

create table airdatas_airline (
	id serial,
	name varchar(255) not null,
	code varchar(31) not null unique,
	country varchar(255),
	archived smallint,
	primary key (id)
);

create table airdatas_flight (
	id bigserial,
	number integer not null unique,
	airline_id integer not null,
	from_airport_id integer not null,
	to_airport_id integer not null,
	takeoff_date timestamp with time zone not null,
	landing_date timestamp with time zone not null,

	primary key (id),
	constraint fk_flight_airline_id foreign key (airline_id) references airdatas_airline(id),
    constraint fk_flight_from_id foreign key (from_airport_id) references airdatas_airport(id),
    constraint fk_flight_to_id foreign key (to_airport_id) references airdatas_airport(id)
);

create index idx_flight_from_airport_id on airdatas_flight (from_airport_id);
create index idx_flight_to_airport_id on airdatas_flight (to_airport_id);
create index idx_flight_takeoff_date on airdatas_flight (takeoff_date);
create index idx_flight_landing_date on airdatas_flight (landing_date);


insert into airdatas_airport (name, code, lat, lon)
values ('London Heathrow', 'LHR', 51.4775, -0.461389),
('Amsterdam Schiphol', 'AMS', 52.3081, 4.76417),
('Paris Charles de Gaulle', 'CDG', 	49.0097, 2.54778),
('Frankfurt', 'FRA', 	50.0331, 	8.57056),
('Sheremetyevo', 'SVO', 55.972500, 37.413056),
('Dubai', 'DXB', 25.252778, 55.364444),
('Athens', 'AIA', 37.936389, 23.947222);

insert into airdatas_airline (name, code, country, archived)
values ('FlyDubai', 'FZ', 'United Arab Emirates', 0),
('Lufthansa', 'LH', 'Germany', 0),
('Air France', 'AF', 'France', 0),
('Ryanair', 'FR', 'Ireland', 0),
('Aeroflot', 'SU', 'Russian Federation', 0),
('British Airways', 'BA', 'United Kingdom', 0),
('AirBerlin', 'AB', 'Germany', 1),
('Transaero', 'UN', 'Russian Federation', 1);


insert into airdatas_flight (number, airline_id, from_airport_id, to_airport_id, takeoff_date, landing_date)
values
(150, 1, 6, 5, '2024-05-29 18:00', '2024-05-29 23:00'),
(180, 2, 4, 3, '2024-06-01 14:25', '2024-06-01 16:15'),
(682, 5, 5, 6, '2024-06-11 22:40', '2024-06-12 02:50'),
(381, 4, 3, 1, '2024-05-23 17:45', '2024-05-23 19:20'),
(68, 4, 2, 1, '2024-05-23 17:45', '2024-05-23 19:20'),
(226, 6, 1, 6, '2024-06-05 18:55', '2024-06-05 00:20'),
(53, 3, 3, 6, '2024-06-05 08:00', '2024-06-05 13:20'),
(90, 4, 4, 1, '2024-06-01 11:45', '2024-06-01 13:05'),
(112, 1, 4, 6, '2024-05-30 03:10', '2024-05-30 08:15'),
(203, 3, 4, 3, '2024-05-28 11:40', '2024-05-28 13:05'),
(311, 6, 1, 7, '2024-06-15 17:10', '2024-06-16 00:35'),
(313, 2, 4, 7, '2024-06-10 11:50', '2024-06-10 17:20'),
(839, 6, 1, 2, '2024-06-01 15:30', '2024-06-01 16:20'),
(230, 4, 1, 3, '2024-06-08 13:30', '2024-06-08 14:25');

